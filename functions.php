<?php

/**
 * @package Boldface\OperationPanamaHat
 */

declare( strict_types = 1 );
namespace Boldface\OperationPanamaHat;

/**
 * Add updater controller to the init hook.
 *
 * @since 0.1
 */
function updater_init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->init();
}
\add_action( 'init', __NAMESPACE__ . '\updater_init' );

/**
 * Filter the modules.
 *
 * @since 0.1
 *
 * @param array Array of modules.
 *
 * @return array The filtered array of modules.
 */
function modules( array $modules ) : array {
  $modules[] = 'googleFonts';
  $modules[] = 'widgets';
  return $modules;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\modules' );

/**
 * Filter the Google fonts.
 *
 * @since 0.2
 *
 * @param array Array of Google fonts.
 *
 * @return array The filtered array of Google fonts.
 */
function googleFonts( array $fonts ) : array {
    return [ 'Didot', 'Open Sans' ];
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Filter the sidebars.
 *
 * @since 0.1
 *
 * @param array Array of sidebars.
 *
 * @return array The filtered array of sidebars.
 */
function sidebars( array $sidebars ) : array {
  $sidebars[] = [
    'id'            => 'footer',
    'name'          => 'Footer',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
  ];
  return $sidebars;
}
\add_filter( 'Boldface\Bootstrap\Models\widgets\sidebars', __NAMESPACE__ . '\sidebars' );

/**
 * Filter the footer. Add aside with contents of active sidebar `footer`.
 *
 * @since 0.1
 *
 * @param string The HTML footer.
 *
 * @return array The filtered HTML footer.
 */
function footer( string $html ) : string {
  if( ! \is_active_sidebar( 'footer' ) ) {
    return $html;
  }
  ob_start();
  \dynamic_sidebar( 'footer' );
  $sidebar = ob_get_clean();
  return sprintf(
    '<aside class="%1$s">%2$s</aside>%3$s',
    'footer-widgets container bg-white d-flex justify-content-start justify-content-md-between align-items-start flex-wrap',
    $sidebar,
    $html
  );
}
\add_filter( 'Boldface\Bootstrap\Views\footer', __NAMESPACE__ . '\footer', 20 );

/**
 * Enqueue Block Editor Assets
 *
 * @since 0.1
 */
function enqueueBlockEditorAssets() {
  \wp_enqueue_script(
    'oph-block-editor-assets',
    \get_stylesheet_directory_uri() . '/assets/js/operation-panama-hat.js',
    [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ]
  );
  \wp_enqueue_style(
    'oph-block-editor-assets',
    \get_stylesheet_directory_uri() . '/assets/css/oph-block-editor-assets.css',
    [ 'wp-edit-blocks' ]
  );
}
\add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\enqueueBlockEditorAssets' );

/**
 * Enqueue Block Assets
 *
 * @since 0.1
 */
function enqueueBlockAssets() {
  \wp_enqueue_style(
    'oph-block-assets',
    \get_stylesheet_directory_uri() . '/assets/css/oph-block-assets.css',
    [ 'wp-blocks' ]
  );
  \wp_enqueue_style(
    'zmdi',
    'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.css',
    [ 'oph-block-assets' ]
  );
}
\add_action( 'enqueue_block_assets', __NAMESPACE__ . '\enqueueBlockAssets' );

/**
 * Enqueue Scripts
 *
 * @since 0.1
 */
function enqueueScripts() {
  \wp_enqueue_script(
    'oph-spacer-dots',
    \get_stylesheet_directory_uri() . '/assets/js/oph-spacer-dots.js',
    [ 'jquery' ]
  );
  \wp_enqueue_script(
    'oph-menu',
    \get_stylesheet_directory_uri() . '/assets/js/oph-menu.js',
    [ 'jquery' ]
  );
}
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueueScripts' );

/**
 * Filter the navigation class.
 *
 * @since 0.1
 *
 * @param string The navigation class.
 *
 * @return string The modified navigation class.
 */
function navigationClass( string $class ) : string {
  $class = str_replace( ' bg-dark', '', $class );
  $class .= ' bg-black';
  return $class;
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigationClass' );

/**
 * Override the use of the logo as the navigation brand.
 *
 * @since 0.1
 *
 * @param string The navigation brand.
 *
 * @return string The modified navigation brand.
 */
function navigationBrand( string $brand ) : string {
  $icon = '';
  if( \has_site_icon() ) {
    $icon = sprintf(
      '<img src="%1$s" alt="Operation Panama Hat Logo" class="img img-fluid img-brand" />',
      \get_site_icon_url()
    );
  }
  return 'Operation Panama Hat' . $icon;
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\brand', __NAMESPACE__ . '\navigationBrand', 20 );

/**
 * Filter the loop class.
 *
 * @since 0.1
 *
 * @param string The loop class.
 *
 * @return string The modified loop class.
 */
function loopClass() {
  return 'container-fluid';
}
\add_filter( 'Boldface\Bootstrap\Views\loop\class', __NAMESPACE__ . '\loopClass' );

/**
 * Remove fixed-bottom from footer class.
 *
 * @since 0.1
 *
 * @param string The footer class.
 *
 * @return string The modified footer class.
 */
function footerClass( string $class ) : string {
  $class = str_replace( 'bg-light', 'bg-white', $class );
  return str_replace( ' fixed-bottom', '', $class );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footerClass' );

/**
 * Center the footer text.
 *
 * @since 0.1
 *
 * @param string The footer wrapper class.
 *
 * @return string The modified footer wrapper class.
 */
function footerWrapperClass( string $class ) : string {
  return $class . ' justify-content-around';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\wrapper\class', __NAMESPACE__ . '\footerWrapperClass' );

/**
 * Filter the footer text.
 *
 * @since 0.1
 *
 * @param string The footer text.
 *
 * @return string The modified footer text.
 */
function footerText( string $text ) : string {
  return sprintf(
    '&copy; %1$s Operation Panama Hat Promotions. All Rights Reserved.',
    date( 'Y' )
  );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 * Use a dark button.
 *
 * @since 0.2.2
 *
 * @param string The button class.
 *
 * @return string The new button class.
 */
function contactFormButtonClass( string $class ) : string {
  return 'btn btn-dark';
}
\add_filter( 'Boldface\Bootstrap\Models\contactForm7\button\class', __NAMESPACE__ . '\contactFormButtonClass' );

/**
 * Disable the Bootstrap REST API.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );

/**
 * Disable the Bootstrap autorow feature.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\Controllers\entry\autorow', '__return_false' );
