/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var _wp$blocks = wp.blocks,
    registerBlockType = _wp$blocks.registerBlockType,
    AlignmentToolbar = _wp$blocks.AlignmentToolbar,
    BlockControls = _wp$blocks.BlockControls,
    InspectorControls = _wp$blocks.InspectorControls;
var _wp$editor = wp.editor,
    RichText = _wp$editor.RichText,
    MediaUpload = _wp$editor.MediaUpload;
var _wp$components = wp.components,
    Button = _wp$components.Button,
    TextControl = _wp$components.TextControl;


wp.hooks.addFilter('blocks.getSaveElement', 'operation-panama-hat/container', operationPanamaHatContainer);
function operationPanamaHatContainer(element, blockType, attributes) {
    if ('core/paragraph' === blockType.name || 'core/quote' === blockType.name || 'core/heading' === blockType.name || 'core/shortcode' === blockType.name || 'core/list' === blockType.name) {
        return wp.element.createElement(
            'div',
            { className: 'container' },
            element
        );
    }
    return element;
}

registerBlockType('operation-panama-hat/video-header', {
    title: 'Video Header',
    icon: 'universal-access-alt',
    category: 'layout',
    useOnce: true,

    attributes: {
        title: {
            type: 'array',
            source: 'children',
            selector: 'h2'
        },
        content: {
            type: 'array',
            source: 'children',
            selector: 'p'
        },
        mediaID: {
            type: 'number'
        },
        mediaURL: {
            type: 'string',
            source: 'attribute',
            selector: 'video',
            attribute: 'src'
        }
    },

    edit: function edit(_ref) {
        var attributes = _ref.attributes,
            className = _ref.className,
            setAttributes = _ref.setAttributes;
        var mediaID = attributes.mediaID,
            mediaURL = attributes.mediaURL,
            title = attributes.title,
            content = attributes.content;

        var onSelectVideo = function onSelectVideo(media) {
            setAttributes({
                mediaURL: media.url,
                mediaID: media.id
            });
        };
        var onChangeTitle = function onChangeTitle(value) {
            setAttributes({ title: value });
        };
        var onChangeContent = function onChangeContent(newContent) {
            setAttributes({ content: newContent });
        };

        return wp.element.createElement(
            'div',
            { className: className },
            wp.element.createElement(RichText, {
                tagName: 'h2',
                placeholder: 'Page Header',
                value: title,
                onChange: onChangeTitle
            }),
            wp.element.createElement(RichText, {
                tagName: 'p',
                placeholder: 'Page Content',
                value: content,
                onChange: onChangeContent
            }),
            wp.element.createElement(MediaUpload, {
                onSelect: onSelectVideo,
                type: 'video',
                value: mediaID,
                render: function render(_ref2) {
                    var open = _ref2.open;
                    return wp.element.createElement(
                        Button,
                        { className: mediaID ? 'image-button' : 'button button-large', onClick: open },
                        !mediaID ? 'Select Video' : wp.element.createElement('video', { src: mediaURL })
                    );
                }
            })
        );
    },
    save: function save(_ref3) {
        var attributes = _ref3.attributes,
            className = _ref3.className;
        var mediaID = attributes.mediaID,
            mediaURL = attributes.mediaURL,
            title = attributes.title,
            content = attributes.content;

        return wp.element.createElement(
            'div',
            { className: className },
            wp.element.createElement('video', { autoplay: true, muted: true, loop: true, src: mediaURL }),
            wp.element.createElement(
                'div',
                { className: 'video-header-text' },
                wp.element.createElement(
                    'h2',
                    null,
                    title
                ),
                wp.element.createElement(
                    'p',
                    null,
                    content
                )
            )
        );
    }
});

registerBlockType('operation-panama-hat/callout', {
    title: 'Callout',
    icon: 'universal-access-alt',
    category: 'layout',

    attributes: {
        title: {
            type: 'array',
            source: 'children',
            selector: 'h2'
        },
        content: {
            type: 'array',
            source: 'children',
            selector: 'p'
        }
    },

    edit: function edit(_ref4) {
        var attributes = _ref4.attributes,
            className = _ref4.className,
            setAttributes = _ref4.setAttributes;
        var title = attributes.title,
            content = attributes.content;

        var onChangeTitle = function onChangeTitle(value) {
            setAttributes({ title: value });
        };
        var onChangeContent = function onChangeContent(newContent) {
            setAttributes({ content: newContent });
        };

        return wp.element.createElement(
            'div',
            { className: className },
            wp.element.createElement(RichText, {
                tagName: 'h2',
                placeholder: 'Page Header',
                value: title,
                onChange: onChangeTitle
            }),
            wp.element.createElement(RichText, {
                tagName: 'p',
                placeholder: 'Page Content',
                value: content,
                onChange: onChangeContent
            })
        );
    },
    save: function save(_ref5) {
        var attributes = _ref5.attributes,
            className = _ref5.className;
        var title = attributes.title,
            content = attributes.content;

        return wp.element.createElement(
            'div',
            { className: className },
            wp.element.createElement(
                'h2',
                null,
                title
            ),
            wp.element.createElement(
                'p',
                null,
                content
            )
        );
    }
});

/***/ })
/******/ ]);