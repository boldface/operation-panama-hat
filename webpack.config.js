module.exports = {
	entry: './assets/js/block.js',
	output: {
		path: __dirname,
		filename: 'assets/js/operation-panama-hat.js',
	},
	module: {
		loaders: [
			{
				test: /.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
			},
		],
	},
};
