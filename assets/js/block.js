const { registerBlockType, AlignmentToolbar, BlockControls, InspectorControls } = wp.blocks;
const { RichText, MediaUpload } = wp.editor;
const { Button, TextControl } = wp.components;

wp.hooks.addFilter(
  'blocks.getSaveElement',
  'operation-panama-hat/container',
  operationPanamaHatContainer
);
function operationPanamaHatContainer( element, blockType, attributes ) {
  if(
    'core/paragraph' === blockType.name
    || 'core/quote' === blockType.name
    || 'core/heading' === blockType.name
    || 'core/shortcode' === blockType.name
    || 'core/list' === blockType.name
  ) {
    return <div className="container">{ element }</div>;
  }
  return element;
}

registerBlockType( 'operation-panama-hat/video-header', {
    title: 'Video Header',
    icon: 'universal-access-alt',
    category: 'layout',
    useOnce: true,

    attributes: {
      title: {
          type: 'array',
          source: 'children',
          selector: 'h2',
      },
      content: {
          type: 'array',
          source: 'children',
          selector: 'p',
      },
        mediaID: {
            type: 'number',
        },
        mediaURL: {
            type: 'string',
            source: 'attribute',
            selector: 'video',
            attribute: 'src',
        },
    },

    edit( { attributes, className, setAttributes } ) {
        const { mediaID, mediaURL, title, content } = attributes;
        const onSelectVideo = media => {
            setAttributes( {
                mediaURL: media.url,
                mediaID: media.id,
            } );
        };
        const onChangeTitle = value => {
            setAttributes( { title: value } );
        };
        const onChangeContent = newContent => {
            setAttributes( { content: newContent } );
        };

        return (
            <div className={ className }>
            <RichText
                tagName="h2"
                placeholder={ 'Page Header' }
                value={ title }
                onChange={ onChangeTitle }
            />
            <RichText
                tagName="p"
                placeholder={ 'Page Content' }
                value={ content }
                onChange={ onChangeContent }
            />
            <MediaUpload
                    onSelect={ onSelectVideo }
                    type="video"
                    value={ mediaID }
                    render={ ( { open } ) => (
                        <Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
                            { ! mediaID ? 'Select Video' : <video src={ mediaURL } /> }
                        </Button>
                    )}
                />
            </div>
        );
    },
    save( { attributes, className } ) {
        const { mediaID, mediaURL, title, content } = attributes;
        return (
            <div className={ className }>
              <video autoplay muted loop src={ mediaURL }/>
              <div className="video-header-text">
                <h2>{ title }</h2>
                <p>{ content }</p>
              </div>
            </div>
        );
    },
} );

registerBlockType( 'operation-panama-hat/callout', {
    title: 'Callout',
    icon: 'universal-access-alt',
    category: 'layout',

    attributes: {
      title: {
          type: 'array',
          source: 'children',
          selector: 'h2',
      },
      content: {
          type: 'array',
          source: 'children',
          selector: 'p',
      },
    },

    edit( { attributes, className, setAttributes } ) {
        const { title, content } = attributes;
        const onChangeTitle = value => {
            setAttributes( { title: value } );
        };
        const onChangeContent = newContent => {
            setAttributes( { content: newContent } );
        };

        return (
            <div className={ className }>
            <RichText
                tagName="h2"
                placeholder={ 'Page Header' }
                value={ title }
                onChange={ onChangeTitle }
            />
            <RichText
                tagName="p"
                placeholder={ 'Page Content' }
                value={ content }
                onChange={ onChangeContent }
            />
          </div>
        );
    },
    save( { attributes, className } ) {
        const { title, content } = attributes;
        return (
            <div className={ className }>
              <h2>{ title }</h2>
              <p>{ content }</p>
            </div>
        );
    },
} );
