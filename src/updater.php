<?php

/**
 * @package Boldface\OperationPanamaHat
 */

declare( strict_types = 1 );
namespace Boldface\OperationPanamaHat;

/**
 * Class for updating the theme
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'operation-panama-hat';
  protected $repository = 'boldface/operation-panama-hat';
}
