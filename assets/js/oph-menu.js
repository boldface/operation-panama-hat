jQuery(document).ready(function() {
  function isLargeScreen() {
    var width = jQuery(window).width();
    var height = jQuery(window).height();
    return ( width > 900 && height > 715 );
  }
  function hasVideoHeader() {
    var header = jQuery('.wp-block-operation-panama-hat-video-header');
    return header.length !== 0;
  }
  function transitionOut() {
    jQuery('.nav-item').each(function(index){
      jQuery(this).css('transition-delay','0s');
      jQuery(this).removeClass('expand');
    });
  }
  function transitionIn() {
    jQuery('.nav-item').each(function(index){
      jQuery(this).css('transition-delay',index/10+'s');
      jQuery(this).addClass('expand');
    });
  }
  function menuOverlay(navbar) {
    jQuery(navbar).removeClass('collapse').addClass('large-screen');
    jQuery(navbar).appendTo('.wp-block-operation-panama-hat-video-header');

    jQuery('#navbarCollapse .nav-link').wrapInner('<span class="nav-text"></span>');
    setTimeout(transitionIn,200);
    setTimeout(transitionOut,1000);
  }
  function resetMenu(navbar) {
    var parent = jQuery(navbar).parent();
    jQuery(navbar).removeClass('large-screen').addClass('collapse');
  }
  function windowResize(navbar) {
    if( ! hasVideoHeader() ) {
      return;
    }
    if( isLargeScreen() ) {
      menuOverlay(navbar);
    }
    else {
      resetMenu(navbar);
    }
  }

  var navbar = jQuery('#navbarCollapse');
  windowResize(navbar);
  jQuery(window).resize(windowResize);
});
